# BUDGET APPLICATION
L’objectif de cette application est de cette application est de permettre à l’utilisateur de gérer son budget et de contrôler ses dépenses quoitidienne.

## Maquette de l'application
l'application est rapide d'utilisation et elle se compose d'une seule page.
Elle est composée de :
* 3 saisis de texte tels que le montant du budget, montant des dépenses et le nom des dépenses.
* 3 donnés chiffré tels que le budget, les dépenses et la balance
* d’un tableau de 2 colonnes qui rentre le nom des dépense et leur montant.
## Page HTML
Notre page HTML est La page HTML est essentiellement composé de bootstrap.
Elle contient aussi des balises responsives pour rendre la maquette adaptable à tout supports 
exemple : « container-fluid » « col md-5 my-3 » etc.
 
## Nos entities :
Pour la création de cette appli nous avons crée 3 classe :
la classe Budget qui nous permets d’affiche notre montant une fois saisie.
La classe Expense qui nous permettait d’affiché aussi le montant des dépense mais aussi le nom de la dépense.
Et la classe Balance qui nous permet de faire la balance entre notre budget et nos dépense.

## Probléme rencontré :
j'ai rencontré quelque Probléme pour la mise en page HTML avec bootstrap j'ai du à coté faire du CSS pour apporté quelque modification.
Par manque de temps je n'ai pas pu mettre en place un code couleur pour le montant de la balance (vert quand elle est positif et rouge quand elle est négatif) avec un If else < 0 et >0.
La mise en place d'un donnés de jeu n'a pas pu etre realisé aussi.
