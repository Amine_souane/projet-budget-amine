import { Balance, Budget, Expense } from './entities'
import './style.css'

const displayExpense = document.querySelector<HTMLTableSectionElement>("tbody")!
let budget= new Budget(0);
const bdg= document.querySelector<HTMLElement>('#budget-amount');
bdg!.textContent=budget.budgetAmount.toString();

let balance= new Balance(0);
const blnc= document.querySelector<HTMLElement>("#balance-amount");
blnc!.textContent=balance.balanceAmount.toString();

function addBudget(){
  const submitBudget= document.querySelector<HTMLInputElement>("#btn-budget")
  submitBudget?.addEventListener('click',(event) => {
    event.preventDefault();
    let inputBudget= document.querySelector<HTMLInputElement>("#budget");
    bdg.textContent=parseInt(inputBudget?.value!) + parseInt(bdg?.textContent!);
    blnc.textContent=parseInt(inputBudget?.value!) + parseInt(blnc?.textContent!);
  })
}
addBudget();

let expense= new Expense(0,"");
const exps= document.querySelector<HTMLElement>('#expense-amount');
exps!.textContent=expense.expenseAmount.toString();

let listExpense:Expense[]=[];

function addExpense(){
  const submitExpense= document.querySelector<HTMLInputElement>("#btn-expense")
  submitExpense?.addEventListener('click',(event) => {
    event.preventDefault();
    let inputExpense= document.querySelector<HTMLInputElement>("#expense");
    let inputExpenseLabel=document.querySelector<HTMLInputElement>("#typeExpense")
    exps.textContent=parseInt(inputExpense?.value!) + parseInt(exps?.textContent!);
    blnc.textContent=parseInt(blnc?.textContent!) - parseInt(inputExpense?.value!);
    if (inputExpense) {
      addListExpense(parseInt(inputExpense.value),inputExpenseLabel!.value)
    }    
    displayExpense.innerHTML=displayExpenseList();
  })

}

function addListExpense(expenseAmount:number, expenseLabel:string){
  listExpense.push({
    expenseAmount:expenseAmount,
    expenseLabel:expenseLabel
  });
  
}

function displayExpenseList(){
  let display = "";
  for (const expense of listExpense){
    display += `
    <tr>
    <td> ${expense.expenseLabel} </td>
    <td> ${expense.expenseAmount} </td> 
    </tr>
    `
  }
  return display;
}

addExpense();

