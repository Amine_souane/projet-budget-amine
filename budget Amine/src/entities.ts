export class Budget {
    budgetAmount:number;

    constructor(budgetAmount:number) {
        this.budgetAmount=0;
    }
}

export class Expense {
    expenseAmount:number;
    expenseLabel:string;

    constructor(expenseAmount:number, expenseLabel:string) {
        this.expenseAmount=0;
        this.expenseLabel=expenseLabel;
    }
}

export class Balance {
    balanceAmount:number;

    constructor(balanceAmount:number) {
        this.balanceAmount=0;
    }
}